from django.urls import include, path
from rest_framework import routers
from tutorial.quickstart import views

"""
 Routers are used with ViewSets in django rest framework to auto config
 the urls. Routers provides a simple, quick and consistent way of
 wiring ViewSet logic to a set of URLs. Router automatically maps
 the incoming request to proper viewset action based on the request
 method type(i.e GET, POST, etc).
"""
router = routers.DefaultRouter()
router.register('users', views.UserViewSet)
router.register('groups', views.GroupViewSet)

"""
Wire up our API using automatic URL routing.Additionally,we include login
URLs for the browsable API.

The namespace attribute allows a group of urls to be identified with a
unique qualifier. Because the namespace attribute 
is associated with a group of urls, it's used in conjunction with the
include method described earlier to consolidate urls. """

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework'))
]