from django.contrib.auth.models import User, Group  # In built models
from rest_framework import viewsets
from tutorial.quickstart.serializers import UserSerializer, GroupSerializer

"""
Using ViewSet you don't have to create separate views for getting list
of objects and detail of one object. ViewSet will handle for you in
consistent way both list and detail.
"""


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
